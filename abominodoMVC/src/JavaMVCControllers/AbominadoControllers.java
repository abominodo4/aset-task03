/// hri 

package JavaMVCControllers;

import java.util.Scanner;

import JavaMVCModels.*;
import JavaMVCViews.*;

public class AbominadoControllers {

	/// View Application Interface
	public void startApplication() {
		AbominodoView view = new AbominodoView();
		view.setVisible(true);
	}

	public String getMessage() {
		try {
			AbominodoModel model = new AbominodoModel();
			return model.getData();
		} catch (Exception er) {
			return "There was an error.";
		}
	}

	public void viewData() {
		Scanner in = new Scanner(System.in);
		System.out.println();
		String h1 = "Main menu";
		String u1 = h1.replaceAll(".", "=");
		System.out.println(u1);
		System.out.println(h1);
		System.out.println(u1);
		System.out.println("1) Play");
		System.out.println("2) View high scores");
		System.out.println("3) View rules");
		System.out.println("0) Quit");
		boolean quit = false;

		int menuItem;
		do {
			System.out.print("Choose menu item: ");
			menuItem = in.nextInt();
			switch (menuItem) {
			case 1:
				System.out.println("You are going to play the game");
				break;
			case 2:
				System.out.println("You are going to View high score");
				break;
			case 3:
				System.out.println("You are going to View rules ");
				break;
			case 0:
				quit = true;
				break;
			default:
				System.out.println("Invalid choice.");
			}
		} while (!quit);
		System.out.println("Don�t you try again?");
		System.out.println("Thankyou for playing");
		System.exit(0);
	}

}
