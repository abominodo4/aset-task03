package JavaMVCViews;

import JavaMVCControllers.*;

public class AbominodoView extends javax.swing.JFrame {
	private AbominadoControllers controller = new AbominadoControllers();

	public AbominodoView() {
		initComponents();
	}

	@SuppressWarnings("unchecked")
	private void initComponents() {

		myLabel = new javax.swing.JLabel();
		loadData = new javax.swing.JButton();
		startMain = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		setName("myFrame");

		myLabel.setText("Press Start to play the game");

		loadData.setText("Start");
		loadData.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				loadDataMouseClicked(evt);
			}
		});

		startMain.setText("Continue");
		startMain.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				startMainMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()

						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(myLabel,
										javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE))
								.addGroup(layout.createSequentialGroup()

										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(layout.createSequentialGroup().addGap(153, 153, 153)
														.addComponent(loadData))
												.addGroup(layout.createSequentialGroup().addGap(147, 147, 147)
														.addComponent(startMain)))
										.addGap(0, 0, Short.MAX_VALUE)))
						.addContainerGap()));

		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(loadData).addGap(26, 26, 26)
						.addComponent(myLabel)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 87, Short.MAX_VALUE)
						.addComponent(startMain).addGap(94, 94, 94)));

		pack();
	}

	private void loadDataMouseClicked(java.awt.event.MouseEvent evt) {
		try {
			String data = controller.getMessage();
			myLabel.setText(data);
			myLabel.setVisible(true);
		} catch (Exception er) {
		}
	}

	private void startMainMouseClicked(java.awt.event.MouseEvent evt) {
		try {
			controller.viewData();
		} catch (Exception er) {
		}
	}

	public static void main(String args[]) {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(AbominodoView.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(AbominodoView.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(AbominodoView.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(AbominodoView.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		}

		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new AbominodoView().setVisible(true);
			}
		});
	}

	public javax.swing.JButton loadData;
	public javax.swing.JLabel myLabel;
	private javax.swing.JButton startMain;
}
