package abominadoStrategy;

interface Strategy {
	public void executeAlgorithm();
}

class ConcreteStrategyA implements Strategy {
	public void executeAlgorithm() {
		System.out.println("Concrete Strategy A");
	}
}

class ConcreteStrategyB implements Strategy {
	public void executeAlgorithm() {
		System.out.println("Concrete Strategy B");
	}
}

class ConcreteStrategyC implements Strategy {
	public void executeAlgorithm() {
		System.out.println("Concrete Strategy C");
	}
}
 