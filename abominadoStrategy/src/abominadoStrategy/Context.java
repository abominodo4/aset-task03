package abominadoStrategy;

public class Context {
	private Strategy strategyA = new ConcreteStrategyA();
	private Strategy strategyB = new ConcreteStrategyB();
	private Strategy strategyC = new ConcreteStrategyC();

	public void executeA() {
		strategyA.executeAlgorithm();
	}

	public void setStrategyA(Strategy strategyA) {
		strategyA = strategyA;
	}

	public Strategy getStrategyA() {
		return strategyA;
	}

	public void executeB() {
		strategyB.executeAlgorithm();
	}

	public void setStrategyB(Strategy strategyB) {
		strategyB = strategyB;
	}

	public Strategy getStrategyB() {
		return strategyB;
	}

	public void executeC() {
		strategyC.executeAlgorithm();
	}

	public void setStrategyC(Strategy strategyC) {
		strategyC = strategyC;
	}

	public Strategy getStrategyC() {
		return strategyC;
	}

}
