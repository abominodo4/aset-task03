package abominadoStrategy;

public class User {

	public static void main(String[] args) {
		Context contextA = new Context();
		contextA.executeA();
		contextA.setStrategyA(new ConcreteStrategyA());

		Context contextB = new Context();
		contextB.executeB();
		contextB.setStrategyB(new ConcreteStrategyB());

		Context contextC = new Context();
		contextC.executeC();
		contextC.setStrategyC(new ConcreteStrategyC());
	}
}